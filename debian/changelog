libsavitar (5.0.0-1) UNRELEASED; urgency=medium

  [ Sebastian Kuzminsky ]
  * New upstream version 5.0.0.
  * Refresh patches.
  * pq 2004: add a patch to let PYVER override Python_VERSION.
  * Build-depend on sip6.
  * Depend on python3-pyqt6.

 -- Sebastian Kuzminsky <seb@highlab.com>  Wed, 25 May 2022 09:28:39 -0600

libsavitar (4.13.0-2) unstable; urgency=medium

  * Depend on python3-pyqt5.

 -- Christoph Berg <myon@debian.org>  Fri, 14 Jan 2022 09:42:49 +0100

libsavitar (4.13.0-1) unstable; urgency=medium

  [ Gregor Riepl ]
  * Mark autopkgtest as superficial. (Closes: #974499)

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on cmake, libpugixml-dev and
      python3-sip-dev.
    + libsavitar-dev: Drop versioned constraint on libpugixml-dev in Depends.

  [ Christoph Berg ]
  * New upstream version 4.13.0.

 -- Christoph Berg <myon@debian.org>  Fri, 14 Jan 2022 09:13:33 +0100

libsavitar (4.8-1) unstable; urgency=medium

  * New upstream version 4.8.
  * Refresh cmake patches, upstream uses PythonInterp/Libs now.
  * debian/watch: Handle -beta releases.
  * Add myself to Uploaders.

 -- Christoph Berg <myon@debian.org>  Mon, 16 Nov 2020 10:11:46 +0100

libsavitar (4.7.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 4.7.1.

 -- Christoph Berg <myon@debian.org>  Sun, 06 Sep 2020 23:01:05 +0200

libsavitar (4.6.2-1) unstable; urgency=low

  * Team upload.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Christoph Berg ]
  * New upstream version.
  * Remove symbols file. Maintaining symbols for C++ libraries requires
    much more effort than it helps in solving actual problems. The latest
    breakage was induced by GCC 10. Also, we don't have any external reverse-
    dependencies anyway. (Closes: #957474)

 -- Christoph Berg <myon@debian.org>  Fri, 31 Jul 2020 19:14:04 +0200

libsavitar (4.5.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.

 -- Christoph Berg <myon@debian.org>  Fri, 13 Mar 2020 22:10:02 +0100

libsavitar (4.4.1-2) unstable; urgency=medium

  * Team upload.

  [ Matthias Klose ]
  * Mark some more symbols as optional. (Closes: #950346)

 -- Christoph Berg <myon@debian.org>  Sun, 02 Feb 2020 21:22:22 +0100

libsavitar (4.4.1-1) unstable; urgency=medium

  * Team upload.

  [ Gregor Riepl ]
  * New upstream version 4.2.0
  * Updated standards
  * Enabled multi-Python build
  * Removed explicit Python 3.5 dependency, bullseye no longer needs it
  * Cleaned up API symbols file

  [ Christoph Berg ]
  * New upstream version 4.4.1.
  * Add patch to force python version in CMake.
  * tests: Depend on all python3 versions.

 -- Christoph Berg <myon@debian.org>  Tue, 28 Jan 2020 10:42:29 +0100

libsavitar (3.3.0-3) unstable; urgency=medium

  [Gregor Riepl]
  * Depend on python3-dev since we're not building for all Python
    interpreters.
    Closes: #909730

  [Matthias Klose]
  * Mark more symbols as optional not seen when building with -O3.
    Closes: #912413

  [ Gregor Riepl ]
  * Added new missing generated symbols (related to #912413).

  [ Petter Reinholdtsen ]
  * Corrected incorrectly blacklisting of python3-sip version 4.19.12+dfsg-1.

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 08 Dec 2018 14:59:14 +0000

libsavitar (3.3.0-2) unstable; urgency=medium

  * Blacklisted incompatible python3-sip version (4.19.11+dfsg-1)
  * Made more symbols optional to allow for -O3 builds on
    Ubuntu (Closes: #903422).

 -- Gregor Riepl <onitake@gmail.com>  Mon, 16 Jul 2018 06:21:29 +0000

libsavitar (3.3.0-1) unstable; urgency=medium

  * New upstream version 3.3.0
  * Migrated to salsa

 -- Gregor Riepl <onitake@gmail.com>  Mon, 18 Jun 2018 18:59:27 +0200

libsavitar (3.2.1-1) unstable; urgency=medium

  * New upstream version 3.2.1
  * Set Multi-Arch:same to enable co-installability

 -- Gregor Riepl <onitake@gmail.com>  Fri, 16 Mar 2018 08:53:18 +0100

libsavitar (3.1.0-1) unstable; urgency=medium

  * New upstream version 3.1.0

 -- Gregor Riepl <onitake@gmail.com>  Sat, 23 Dec 2017 12:39:22 +0100

libsavitar (3.0.3-3) unstable; urgency=medium

  [ Gregor Riepl ]
  * Fixed incorrect quoting in symbols file (Closes: #881493).
  * Some copyright file corrections (Closes: #881483).

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 21 Nov 2017 02:25:24 +0000

libsavitar (3.0.3-2) unstable; urgency=medium

  [ Gregor Riepl ]
  * Removed license deviation for src/Face.h and added clarification about the
    incorrect license header
  * Symbols update for archs other than amd64

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 13 Nov 2017 15:10:17 +0100

libsavitar (3.0.3-1) unstable; urgency=medium

  [ Gregor Riepl ]
  * Updated to version 3.0.3.
  * Upstream license change from AGPL-3+ to LGPL-3+.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 29 Oct 2017 07:27:58 +0000

libsavitar (2.6.0-1) unstable; urgency=medium

  [ Gregor Riepl ]
  * Initial Debian release. See #706656.
  * Based on upstream release 2.6.0, as this version includes patches that
    separate the C++ library from the Python SIP module.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 08 Oct 2017 06:02:13 +0000
